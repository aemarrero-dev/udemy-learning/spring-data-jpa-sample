package net.itinajero.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.itinajero.model.Perfil;
import net.itinajero.repository.IPerfilesRepository;

@Service
public class PerfilesServiceImpl implements IPerfilesService {

	@Autowired
	private IPerfilesRepository repo;
	
	@Override
	public List<Perfil> buscarTodas() {
		List<Perfil> lista = repo.findAll();
		return lista;
	}

}
