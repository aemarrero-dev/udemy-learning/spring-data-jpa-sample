package net.itinajero.service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import net.itinajero.model.Categoria;
import net.itinajero.repository.ICategoriasRepository;

@Service
public class CategoriasServiceImpl implements ICategoriasService {

	@Autowired
	private ICategoriasRepository repoCategorias;
	
	@Override
	//PERSISTIR UN OBJETO- SAVE()
	public void guardar() {
		Categoria c = new Categoria();
		c.setNombre("Construccion");
		c.setDescripcion("Construccion y Mantenimiento");
		repoCategorias.save(c);
	}

	@Override
	//USO DE FINDBYID
	public void buscarPorId(int id) {
		Optional<Categoria> op = repoCategorias.findById(id);
		if(op.isPresent())
			System.out.println("Categoria: " + op.get().toString());
		else 
			System.out.println("NOT FOUND");
	}

	@Override
	//USO DE SAVE PARA ACTUALIZAR
	public void actualizar(int id) {
		Optional<Categoria> op = repoCategorias.findById(id);
		if(op.isPresent()) {
			Categoria categoria = op.get();
			categoria.setNombre("Financiera");
			categoria.setDescripcion("Banca y Finanzas");
			repoCategorias.save(categoria);
		}
		else 
			System.out.println("NOT FOUND");

	}

	@Override
	//USO DE DELETE
	public void borrar(int id) {
		Optional<Categoria> op = repoCategorias.findById(id);
		if(op.isPresent()) {
			repoCategorias.deleteById(id);
		}
		else 
			System.out.println("NOT FOUND");
	}

	@Override
	//USO DE COUNT
	public void nroRegistros() {
		System.out.println("NRO REGISTROS: " + repoCategorias.count());
	}

	@Override
	//USO DE FINDALLBYID
	public void buscarPorIds() {
		List<Integer> ids = new LinkedList<Integer>();
		ids.add(12);
		ids.add(13);
		ids.add(14);
		ids.add(15);
		Iterable<Categoria> lista = repoCategorias.findAllById(ids);
		for (Categoria categoria : lista) {
			System.out.println(categoria.toString());
		}
	}

	@Override
	//USO DE FINDALL
	public List<Categoria> buscarTodas() {
		List<Categoria> lista = repoCategorias.findAll();
		return lista;
	}

	@Override
	//USO DE FINDALL CON ORDENAMIENTO
	public void buscarTodasOrdenadas() {
		Iterable<Categoria> lista = repoCategorias.findAll(Sort.by("id").descending());
		for (Categoria categoria : lista) {
			System.out.println(categoria.toString());
		}
	}

	@Override
	//USO DE FINDALL CON PAGINACION
	public void buscarTodasPaginacion() {
		//Obtenemos primera pagina con los primeros 5 elementos.
		//Page<Categoria> pages = repo.findAll(PageRequest.of(0, 5));
		
		//FindAll con Paginacion y Ordenamiento
		Page<Categoria> pages = repoCategorias.findAll(PageRequest.of(0, 5, Sort.by("id").descending()));
		
		System.out.println("Total de Registros: " + pages.getTotalElements());
		System.out.println("Total de Paginas: " + pages.getTotalPages());
		
		for (Categoria categoria : pages.getContent()) {
			System.out.println(categoria.toString());
		}
	}

	@Override
	//USO DE EXISTBYID
	public void existe() {
		System.out.println("Id 12 : " + repoCategorias.existsById(12));
		System.out.println("Id 16 : " + repoCategorias.existsById(16));
	}

	@Override
	//USO DE SAVEALL
	public void guardarVarios() {
		List<Categoria> lista = new LinkedList<Categoria>();
		Categoria c1 = new Categoria();
		c1.setNombre("Gubernamental");
		c1.setDescripcion("Gobierno");
		Categoria c2 = new Categoria();
		c2.setNombre("Ingenieria");
		c2.setDescripcion("Area de Manufactura");
		
		lista.add(c1);
		lista.add(c2);
		
		repoCategorias.saveAll(lista);
	}

}
