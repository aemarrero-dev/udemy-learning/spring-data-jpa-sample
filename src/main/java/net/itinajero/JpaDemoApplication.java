package net.itinajero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.util.Date;
import java.util.List;

import net.itinajero.model.*;
import net.itinajero.service.ICategoriasService;
import net.itinajero.service.IPerfilesService;
import net.itinajero.service.IUsuariosService;
import net.itinajero.service.IVacantesService;

//La interfaz CommandLineRunner convierte esta aplicacion de tipo consola.

@SpringBootApplication
public class JpaDemoApplication implements CommandLineRunner {
	
	@Autowired
	private ICategoriasService categoriasService;
	
	@Autowired
	private IVacantesService vacantesService;
	
	@Autowired
	private IPerfilesService perfilesService;
	
	@Autowired
	private IUsuariosService usuariosService;
	
	public static void main(String[] args) {
		SpringApplication.run(JpaDemoApplication.class, args);
	}
	
	//El metodo run es el inicio y fin del programa.
	@Override
	public void run(String... args) throws Exception {
		System.out.println("Ejemplos de Spring Data JPA");
		
		/* CONSULTA DE CATEGORIAS */
		//listarCategorias();
		
		/* CONSULTA DE VACANTES */
		//listarVacantes();
		//vacantesService.guardar(nuevaVacante());
		//buscarVacantesPorEstatus("Eliminada");
		//buscarVacantesPorDestacado_Estatus_OrdenadoPorId_Desc(1, "Creada");
		//buscarVacantesPorRangoSalario(7000, 14000);
		
		String[] estatus = new String[]{"Eliminada", "Creada"};
		buscarVacantesEntreEstatus(estatus);
		
		
		/* CONSULTA DE PERFILES */
		//listarPerfiles();
		
		/* CONSULTA DE USUARIOS */
		//listarUsuarios();
		//guardarUsuarioConPerfil();
		//buscarUsuarioPorId(2);

		System.exit(0);	
	}
	
	//Creamos un objeto vacante
	private Vacante nuevaVacante() {
		Vacante v = new Vacante();
		v.setNombre("Analista QA");
		v.setDescripcion("Profesional en el area de testing");
		v.setFecha(new Date());
		v.setSalario(1700000.0);
		v.setEstatus("Aprobada");
		v.setDestacado(0);
		v.setImagen("noimage.png");
		v.setDetalles("<h1>QA, Testing, Pruebas Automatizadas<h1/>");
		Categoria categoria = new Categoria();
		categoria.setId(3); //Asociamos esta vacante a la categoria ya existente con Id=3
		v.setCategoria(categoria);
		return v;
	}
	
	private void listarCategorias() {
		List<Categoria> lista = categoriasService.buscarTodas();
		for (Categoria categoria : lista) {
			System.out.println(categoria.toString());
		}
	}
	
	private void listarVacantes() {
		List<Vacante> lista = vacantesService.buscarTodas();
		for (Vacante vacante : lista) {
			System.out.println("Vacante-" + vacante.getId() + ": " + vacante.getNombre() + " - Categoria-" + vacante.getCategoria().getId());
		}
	}
	
	private void listarPerfiles() {
		List<Perfil> lista = perfilesService.buscarTodas();
		for (Perfil perfil : lista) {
			System.out.println(perfil.toString());
		}
	}
	
	private void listarUsuarios() {
		List<Usuario> lista = usuariosService.buscarTodas();
		for (Usuario usuario : lista) {
			System.out.println(usuario.toString());
		}
	}
	
	//Lista usuarios y sus perfiles
	private void buscarUsuarioPorId(int id) {
		Usuario u = usuariosService.buscarPorId(id);
		System.out.println(u.toString());
		System.out.println("Perfiles:");
		for(Perfil p : u.getPerfiles()) {
			System.out.println(p.toString());
		}
		
	}
	
	//Guarda Usuario con algunos perfiles
	private void guardarUsuarioConPerfil() {
		Usuario u = new Usuario();
		u.setNombre("Andres Marrero");
		u.setEmail("aemarrero132@gmail.com");
		u.setFechaRegistro(new Date());
		u.setUsername("andres");
		u.setPassword("andres123");
		u.setEstatus(1);
		
		Perfil p1 = new Perfil();
		p1.setId(2);
		Perfil p2 = new Perfil();
		p2.setId(3);
		
		u.agregar(p1);
		u.agregar(p2);
		
		usuariosService.guardar(u);
		
	}
	
	//BUSCA VACANTES POR ESTATUS
	private void buscarVacantesPorEstatus(String estatus) {
		List<Vacante> lista = vacantesService.buscarPorEstatus(estatus);
		for (Vacante vacante : lista) {
			System.out.println("Vacante #" + vacante.getId() + ". Estatus: " + vacante.getEstatus());
		}
	}
	
	//BUSCA VACANTES POR ESTATUS
	private void buscarVacantesPorDestacado_Estatus_OrdenadoPorId_Desc(int destacado, String estatus) {
		List<Vacante> lista = vacantesService.findByDestacadoAndEstatusOrderByIdDesc(destacado, estatus);
		for (Vacante vacante : lista) {
			System.out.println("Vacante #" + vacante.getId() + ". Destacado: " + vacante.getDestacado() + ". Estatus: " + vacante.getEstatus());
		}
	}
	
	//BUSCA VACANTES ENTRE UN RANGO DE SALARIOS
	private void buscarVacantesPorRangoSalario(double salario1, double salario2) {
		List<Vacante> lista = vacantesService.buscarPorRangoSalario(salario1, salario2);
		for (Vacante vacante : lista) {
			System.out.println("Vacante #" + vacante.getId() + ". Salario: $" + vacante.getSalario());
		}
	}
	
	//BUSCA VACANTES ENTRE UN RANGO DE SALARIOS
	private void buscarVacantesEntreEstatus(String...estatus) {
		List<Vacante> lista = vacantesService.buscarEntreEstatus(estatus);
		for (Vacante vacante : lista) {
			System.out.println("Vacante #" + vacante.getId() + ". Estatus: $" + vacante.getEstatus());
		}
	}

}
