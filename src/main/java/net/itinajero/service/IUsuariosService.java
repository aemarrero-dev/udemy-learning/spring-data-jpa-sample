package net.itinajero.service;

import java.util.List;
import net.itinajero.model.Usuario;

public interface IUsuariosService {

	public List<Usuario> buscarTodas();
	public void guardar(Usuario usuario);
	public Usuario buscarPorId(int id);
}
