package net.itinajero.service;

import java.util.List;

import net.itinajero.model.Vacante;

public interface IVacantesService {

	//USO DE FINDALL
	public List<Vacante> buscarTodas();
	
	//USO DE SAVE
	public void guardar(Vacante v);
	
	//LLAMADA A QUERY METHOD
	public List<Vacante> buscarPorEstatus(String estatus);
	
	//LLAMADA A QUERY METHOD
	public List<Vacante> findByDestacadoAndEstatusOrderByIdDesc(int destacado, String estatus);
	
	//LLAMADA A QUERY METHOD
	public List<Vacante> buscarPorRangoSalario(double salario1, double salario2);
	
	//LLAMADA A QUERY METHOD
	public List<Vacante> buscarEntreEstatus(String ...estatus );
	
}
