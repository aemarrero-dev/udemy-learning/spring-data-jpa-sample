package net.itinajero.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.itinajero.model.Usuario;
import net.itinajero.repository.IUsuariosRepository;

@Service
public class UsuariosServiceImpl implements IUsuariosService {

	@Autowired
	private IUsuariosRepository repo;
	
	@Override
	public List<Usuario> buscarTodas() {
		List<Usuario> lista = repo.findAll();
		return lista;
	}

	@Override
	public void guardar(Usuario usuario) {
		repo.save(usuario);
	}

	@Override
	public Usuario buscarPorId(int id) {
		Optional<Usuario> op = repo.findById(id);
		if(op.isPresent()) {
			return op.get();
		}
		return null;
	}

}
