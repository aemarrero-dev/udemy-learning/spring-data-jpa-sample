package net.itinajero.service;

import java.util.List;

import net.itinajero.model.Perfil;

public interface IPerfilesService {
	public List<Perfil> buscarTodas();
}
