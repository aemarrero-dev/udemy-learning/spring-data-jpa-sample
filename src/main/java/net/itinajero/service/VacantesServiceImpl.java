package net.itinajero.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.itinajero.model.Vacante;
import net.itinajero.repository.IVacantesRepository;

@Service
public class VacantesServiceImpl implements IVacantesService{

	@Autowired
	private IVacantesRepository repo;
	
	@Override
	public List<Vacante> buscarTodas() {
		List<Vacante> lista = repo.findAll();
		return lista;
	}

	@Override
	public void guardar(Vacante v) {
		repo.save(v);
	}

	@Override
	public List<Vacante> buscarPorEstatus(String estatus) {
		return repo.findByEstatus(estatus);
	}

	@Override
	public List<Vacante> findByDestacadoAndEstatusOrderByIdDesc(int destacado, String estatus) {
		return repo.findByDestacadoAndEstatusOrderByIdDesc(destacado, estatus);
	}

	@Override
	public List<Vacante> buscarPorRangoSalario(double salario1, double salario2) {
		return repo.findBySalarioBetweenOrderBySalarioDesc(salario1, salario2);
	}

	@Override
	public List<Vacante> buscarEntreEstatus(String... estatus) {
		return repo.findByEstatusIn(estatus);
	}

}
