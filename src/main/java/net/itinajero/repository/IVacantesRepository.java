package net.itinajero.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.itinajero.model.Vacante;

public interface IVacantesRepository extends JpaRepository<Vacante, Integer> {

	//select * from vacantes where estatus = ?
	List<Vacante> findByEstatus(String estatus);
	
	//select * from vacantes where destacado ? and estatus = ? order by id desc
	List<Vacante> findByDestacadoAndEstatusOrderByIdDesc(Integer destacado, String estatus);
	
	//select * from vacantes where salario between ? and ?
	List<Vacante> findBySalarioBetweenOrderBySalarioDesc(double salario1, double salario2);
	
	//select * from vacantes where id in (?, ?, ?)
	List<Vacante> findByEstatusIn(String...estatus);
}

/* QUERY METHODS
 * Son metodos que solo se declaran en la interfaz. Spring Data JPA realiza la implementacion del metodo,
 * dependiendo del NOMBRE DEL METODO y los PARAMETROS que recibe
 *
 */