package net.itinajero.service;

import java.util.List;

import net.itinajero.model.Categoria;

public interface ICategoriasService {

	//PERSISTIR UN OBJETO- SAVE()
	public void guardar();
	
	//USO DE FINDBYID
	public void buscarPorId(int id);
	
	//USO DE UPDATE
	public void actualizar(int id);
	
	//USO DE UPDATE
	public void borrar(int id);
	
	//USO DE COUNT
	public void nroRegistros();
	
	//USO DE FINDALLBYID
	public void buscarPorIds();
	
	//USO DE FINDALL
	public List<Categoria> buscarTodas();
	
	//USO DE FINDALL con SORT
	public void buscarTodasOrdenadas();
	
	//USO DE FINDALL con SORT
	public void buscarTodasPaginacion();
	
	//USO DE EXISTBYID
	public void existe();

	//USO DE SAVEALL
	public void guardarVarios();

}
